# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/home/yedarm/mysql-5.6.24/libservices/my_plugin_log_service.c" "/home/yedarm/mysql-5.6.24/libservices/CMakeFiles/mysqlservices.dir/my_plugin_log_service.c.o"
  "/home/yedarm/mysql-5.6.24/libservices/my_snprintf_service.c" "/home/yedarm/mysql-5.6.24/libservices/CMakeFiles/mysqlservices.dir/my_snprintf_service.c.o"
  "/home/yedarm/mysql-5.6.24/libservices/my_thread_scheduler_service.c" "/home/yedarm/mysql-5.6.24/libservices/CMakeFiles/mysqlservices.dir/my_thread_scheduler_service.c.o"
  "/home/yedarm/mysql-5.6.24/libservices/mysql_string_service.c" "/home/yedarm/mysql-5.6.24/libservices/CMakeFiles/mysqlservices.dir/mysql_string_service.c.o"
  "/home/yedarm/mysql-5.6.24/libservices/thd_alloc_service.c" "/home/yedarm/mysql-5.6.24/libservices/CMakeFiles/mysqlservices.dir/thd_alloc_service.c.o"
  "/home/yedarm/mysql-5.6.24/libservices/thd_wait_service.c" "/home/yedarm/mysql-5.6.24/libservices/CMakeFiles/mysqlservices.dir/thd_wait_service.c.o"
  )
SET(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_CONFIG_H"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
