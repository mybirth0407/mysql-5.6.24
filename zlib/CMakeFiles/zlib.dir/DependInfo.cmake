# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/home/yedarm/mysql-5.6.24/zlib/adler32.c" "/home/yedarm/mysql-5.6.24/zlib/CMakeFiles/zlib.dir/adler32.c.o"
  "/home/yedarm/mysql-5.6.24/zlib/compress.c" "/home/yedarm/mysql-5.6.24/zlib/CMakeFiles/zlib.dir/compress.c.o"
  "/home/yedarm/mysql-5.6.24/zlib/crc32.c" "/home/yedarm/mysql-5.6.24/zlib/CMakeFiles/zlib.dir/crc32.c.o"
  "/home/yedarm/mysql-5.6.24/zlib/deflate.c" "/home/yedarm/mysql-5.6.24/zlib/CMakeFiles/zlib.dir/deflate.c.o"
  "/home/yedarm/mysql-5.6.24/zlib/gzio.c" "/home/yedarm/mysql-5.6.24/zlib/CMakeFiles/zlib.dir/gzio.c.o"
  "/home/yedarm/mysql-5.6.24/zlib/infback.c" "/home/yedarm/mysql-5.6.24/zlib/CMakeFiles/zlib.dir/infback.c.o"
  "/home/yedarm/mysql-5.6.24/zlib/inffast.c" "/home/yedarm/mysql-5.6.24/zlib/CMakeFiles/zlib.dir/inffast.c.o"
  "/home/yedarm/mysql-5.6.24/zlib/inflate.c" "/home/yedarm/mysql-5.6.24/zlib/CMakeFiles/zlib.dir/inflate.c.o"
  "/home/yedarm/mysql-5.6.24/zlib/inftrees.c" "/home/yedarm/mysql-5.6.24/zlib/CMakeFiles/zlib.dir/inftrees.c.o"
  "/home/yedarm/mysql-5.6.24/zlib/trees.c" "/home/yedarm/mysql-5.6.24/zlib/CMakeFiles/zlib.dir/trees.c.o"
  "/home/yedarm/mysql-5.6.24/zlib/uncompr.c" "/home/yedarm/mysql-5.6.24/zlib/CMakeFiles/zlib.dir/uncompr.c.o"
  "/home/yedarm/mysql-5.6.24/zlib/zutil.c" "/home/yedarm/mysql-5.6.24/zlib/CMakeFiles/zlib.dir/zutil.c.o"
  )
SET(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_CONFIG_H"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "include"
  "zlib"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
