# Install script for directory: /home/yedarm/mysql-5.6.24/include

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr/local/mysql-5.6.24")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Development")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE FILE FILES
    "/home/yedarm/mysql-5.6.24/include/mysql.h"
    "/home/yedarm/mysql-5.6.24/include/mysql_com.h"
    "/home/yedarm/mysql-5.6.24/include/mysql_time.h"
    "/home/yedarm/mysql-5.6.24/include/my_list.h"
    "/home/yedarm/mysql-5.6.24/include/my_alloc.h"
    "/home/yedarm/mysql-5.6.24/include/typelib.h"
    "/home/yedarm/mysql-5.6.24/include/mysql/plugin.h"
    "/home/yedarm/mysql-5.6.24/include/mysql/plugin_audit.h"
    "/home/yedarm/mysql-5.6.24/include/mysql/plugin_ftparser.h"
    "/home/yedarm/mysql-5.6.24/include/mysql/plugin_validate_password.h"
    "/home/yedarm/mysql-5.6.24/include/my_dbug.h"
    "/home/yedarm/mysql-5.6.24/include/m_string.h"
    "/home/yedarm/mysql-5.6.24/include/my_sys.h"
    "/home/yedarm/mysql-5.6.24/include/my_xml.h"
    "/home/yedarm/mysql-5.6.24/include/mysql_embed.h"
    "/home/yedarm/mysql-5.6.24/include/my_pthread.h"
    "/home/yedarm/mysql-5.6.24/include/decimal.h"
    "/home/yedarm/mysql-5.6.24/include/errmsg.h"
    "/home/yedarm/mysql-5.6.24/include/my_global.h"
    "/home/yedarm/mysql-5.6.24/include/my_net.h"
    "/home/yedarm/mysql-5.6.24/include/my_getopt.h"
    "/home/yedarm/mysql-5.6.24/include/sslopt-longopts.h"
    "/home/yedarm/mysql-5.6.24/include/my_dir.h"
    "/home/yedarm/mysql-5.6.24/include/sslopt-vars.h"
    "/home/yedarm/mysql-5.6.24/include/sslopt-case.h"
    "/home/yedarm/mysql-5.6.24/include/sql_common.h"
    "/home/yedarm/mysql-5.6.24/include/keycache.h"
    "/home/yedarm/mysql-5.6.24/include/m_ctype.h"
    "/home/yedarm/mysql-5.6.24/include/my_attribute.h"
    "/home/yedarm/mysql-5.6.24/include/my_compiler.h"
    "/home/yedarm/mysql-5.6.24/include/mysql_com_server.h"
    "/home/yedarm/mysql-5.6.24/include/my_byteorder.h"
    "/home/yedarm/mysql-5.6.24/include/byte_order_generic.h"
    "/home/yedarm/mysql-5.6.24/include/byte_order_generic_x86.h"
    "/home/yedarm/mysql-5.6.24/include/byte_order_generic_x86_64.h"
    "/home/yedarm/mysql-5.6.24/include/little_endian.h"
    "/home/yedarm/mysql-5.6.24/include/big_endian.h"
    "/home/yedarm/mysql-5.6.24/include/mysql_version.h"
    "/home/yedarm/mysql-5.6.24/include/my_config.h"
    "/home/yedarm/mysql-5.6.24/include/mysqld_ername.h"
    "/home/yedarm/mysql-5.6.24/include/mysqld_error.h"
    "/home/yedarm/mysql-5.6.24/include/sql_state.h"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Development")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Development")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/mysql" TYPE DIRECTORY FILES "/home/yedarm/mysql-5.6.24/include/mysql/" REGEX "/[^/]*\\.h$" REGEX "/psi\\_abi[^/]*$" EXCLUDE)
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Development")

