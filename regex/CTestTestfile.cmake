# CMake generated Testfile for 
# Source directory: /home/yedarm/mysql-5.6.24/regex
# Build directory: /home/yedarm/mysql-5.6.24/regex
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
ADD_TEST(regex1 "re" "-I")
ADD_TEST(regex2 "re" "-el" "-I")
ADD_TEST(regex3 "re" "-er" "-I")
